package com.bancomysql.mysql.conexao;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexao {

    private static String url = "jdbc:mysql://localhost:3306/bancoibm";
    private static String user = "root";
    private static String pass = "";

    public static Connection getConexao() throws Exception{
        return DriverManager.getConnection(url,user,pass);

    }
}
