package com.bancomysql.mysql.beans;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pessoa {

    @Id
    private Integer id;

    private String nome;

    private String data_nascimento;

    private String telefone;

    private String documento;

    public Pessoa(int codigo, String nome, String data_nascimento, String telefone, String documento) {
    }

    public Pessoa() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(String data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "id=" + id + ",\n\t nome=" + nome + ",\n\t data_nascimento=" + data_nascimento + ",\n\t telefone=" + telefone + ",\n\t documento=" + documento + '}';
    }

}
