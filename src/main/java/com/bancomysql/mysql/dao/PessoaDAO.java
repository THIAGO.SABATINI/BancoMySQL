package com.bancomysql.mysql.dao;

import com.bancomysql.mysql.beans.Pessoa;
import com.bancomysql.mysql.excecao.Excecao;


import java.sql.*;
import java.util.List;

public class PessoaDAO {


    public final String DRIVER = "com.mysql.cj.jdbc.Driver";
    public final String URL = "jdbc:mysql://localhost:3306/bancoibm";
    public final String USER = "root";
    public final String PASSWORD = "";

    public PessoaDAO() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Driver do banco de dados nao encontrado");
        }
    }


    public Pessoa pessoaMap(ResultSet rs) throws SQLException {
        Pessoa pessoa = new Pessoa(rs.getInt("id"), rs.getString("Nome"), rs.getString("data_nascimento"), rs.getString("documento"), rs.getString("telefone"));
        pessoa.setId(rs.getInt("id"));
        pessoa.setNome(rs.getString("Nome"));
        pessoa.setData_nascimento(rs.getString("Data_Nascimento"));
        pessoa.setDocumento(rs.getString("documento"));
        pessoa.setTelefone(rs.getString("telefone"));
        return pessoa;
    }

    //Select por id
    public Pessoa getPessoaId(Integer id) throws Exception{
        Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
        PreparedStatement pst = con.prepareStatement
                ("SELECT * FROM  pessoas WHERE id = ?");
        pst.setInt(1, id);
        ResultSet rs = pst.executeQuery();
        if(rs.next()) {
            return pessoaMap(rs);
        } else {
            System.out.println("Pessoa nao cadastrada");
            return new Pessoa();
        }
    }

    //Selecionar por primeiro nome
    public List<Pessoa> pegarPorNome(String nome){
        final String sql = "SELECT * FROM pessoas WHERE nome LIKE '"+nome+"%'";
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try(Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery()) {
            while (rs.next()){
                pessoas.add(pessoaMap(rs));
            }
            return pessoas;
        } catch(Exception ex){
            Excecao.tratarExcecao(ex);
        }
        return pessoas;
    }

    //Seleciona lista de pessoas por data
    public List<Pessoa> getPessoasData(int ano){
        final String sql = "SELECT * FROM pessoas WHERE year(data_nascimento)>" + ano;
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try(Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
        PreparedStatement pst = con.prepareStatement(sql);
        ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                pessoas.add(pessoaMap(rs));
            }
            return pessoas;
        } catch (Exception ex){
            Excecao.tratarExcecao(ex);
        }
        return pessoas;
    }

    //Selecionar todas as pessoas cadastradas
    public List<Pessoa> findAll() {
        final String sql = "SELECT * FROM pessoas";
        List<Pessoa> pessoas = new java.util.ArrayList<>();
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement pst = con.prepareStatement(sql);
             ResultSet rs = pst.executeQuery()) {
            while (rs.next()) {
                pessoas.add(pessoaMap(rs));
            }
        } catch (SQLException ex) {
            Excecao.tratarExcecao(ex);
        }
        return pessoas;
    }


}
