package com.bancomysql.mysql.pricipal;

import com.bancomysql.mysql.beans.Pessoa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class testeConexao {
    private static Connection conexao;
    private static PreparedStatement stmt;
    private static ResultSet resultSet;
    private static String url = "jdbc:mysql://localhost:3306/bancoibm";
    private static String user = "root";
    private static String pass = "";

    public static void main(String[] args) {

        try{
            conexao = DriverManager.getConnection(url, user, pass);
            stmt = conexao.prepareStatement("SELECT NOME FROM pessoas WHERE ID=1");
            resultSet = stmt.executeQuery();
            if (resultSet.next()){
                String nome = resultSet.getString("nome");
                System.out.println(nome);
            }
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
