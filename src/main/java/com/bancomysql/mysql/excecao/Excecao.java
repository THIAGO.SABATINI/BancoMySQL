package com.bancomysql.mysql.excecao;

public class Excecao extends Exception{

    public static String tratarExcecao (Exception excecao){
        if(excecao.getClass().toString().equals("class java.lang.NumberFormatException")){
            return "Numero ou digito invalido";
        } else if (excecao.getClass().toString().equals("class java.lang.NullPointerException")) {
            return "Campo vazio";
        } else {
            return "Erro inesperado";
        }
    }
}
