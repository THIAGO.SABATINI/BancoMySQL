package com.bancomysql.mysql.controller;

import com.bancomysql.mysql.beans.Pessoa;
import com.bancomysql.mysql.dao.PessoaDAO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PessoaController {

    private PessoaDAO pessoaDAO= new PessoaDAO();

    public PessoaController() throws Exception {
    }

    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas() throws Exception {
        return pessoaDAO.findAll();
    }

    @RequestMapping(value = "id/{id}", method = RequestMethod.GET)
    public Pessoa obterPorId(@PathVariable(value= "id") Integer id) throws Exception {
        return pessoaDAO.getPessoaId(id);
    }

    @RequestMapping(value = "nome/{nome}", method = RequestMethod.GET)
    public List<Pessoa> obterPorNome(@PathVariable(value = "nome") String nome) throws Exception{
        return pessoaDAO.pegarPorNome(nome);
    }

    @RequestMapping(value = "data/{ano}", method = RequestMethod.GET)
    public List<Pessoa> obterPorData(@PathVariable(value = "ano") int ano)throws Exception{
        return pessoaDAO.getPessoasData(ano);
    }


}
